{
    "id": "f301f6ce-2d30-4808-a979-4bc3885e323b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f53494d-3688-4643-b510-973707542ba9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f301f6ce-2d30-4808-a979-4bc3885e323b",
            "compositeImage": {
                "id": "8a02a87d-067b-4571-96c1-d0d5a741d9ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f53494d-3688-4643-b510-973707542ba9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee9d2df4-c6bb-4903-a2eb-7b5e48acc20d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f53494d-3688-4643-b510-973707542ba9",
                    "LayerId": "c0d061cd-239b-49c2-9436-411ee6bc5b0c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c0d061cd-239b-49c2-9436-411ee6bc5b0c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f301f6ce-2d30-4808-a979-4bc3885e323b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 2,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 31,
    "yorig": 0
}