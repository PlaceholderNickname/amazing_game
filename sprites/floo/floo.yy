{
    "id": "9b14130f-65c6-447d-bfdb-121c018ebb02",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "floo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f098678-02f0-4ee0-a202-556ebfe7c085",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b14130f-65c6-447d-bfdb-121c018ebb02",
            "compositeImage": {
                "id": "5d34aed5-4bb1-4fd9-a494-57b856157fe0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f098678-02f0-4ee0-a202-556ebfe7c085",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cf2fee1-0b15-4f20-9dc9-0e5699e76cfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f098678-02f0-4ee0-a202-556ebfe7c085",
                    "LayerId": "ed571a9c-1f31-4717-8589-734dc7575238"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ed571a9c-1f31-4717-8589-734dc7575238",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b14130f-65c6-447d-bfdb-121c018ebb02",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}