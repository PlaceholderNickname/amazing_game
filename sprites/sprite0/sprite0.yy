{
    "id": "0b046aab-4c33-4934-95c4-f476b527205f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4e5e42aa-1b07-46f8-8d1f-ba0f5b36e7e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b046aab-4c33-4934-95c4-f476b527205f",
            "compositeImage": {
                "id": "fdb7befa-9b92-48d7-9496-c42078a6fe59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e5e42aa-1b07-46f8-8d1f-ba0f5b36e7e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03886156-b3e3-4089-a39e-4aa9cdc5f464",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e5e42aa-1b07-46f8-8d1f-ba0f5b36e7e8",
                    "LayerId": "b31f681b-4a0f-4f69-aa9e-723b87ddd92f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b31f681b-4a0f-4f69-aa9e-723b87ddd92f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b046aab-4c33-4934-95c4-f476b527205f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 32,
    "yorig": 0
}