{
    "id": "4d57fd59-bcc7-42ff-8a64-c6a2451f67d4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object0",
    "eventList": [
        {
            "id": "3642cf63-9774-4164-8ff7-21879d0be6ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "4d57fd59-bcc7-42ff-8a64-c6a2451f67d4"
        },
        {
            "id": "5672be0f-cc45-464d-a6dc-9f025f933d33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "4d57fd59-bcc7-42ff-8a64-c6a2451f67d4"
        },
        {
            "id": "6402a58d-4b3b-40b3-b5b0-401f0f0996e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "4d57fd59-bcc7-42ff-8a64-c6a2451f67d4"
        },
        {
            "id": "c11f8cec-ccfe-41ad-99d2-3189d81f2705",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 5,
            "m_owner": "4d57fd59-bcc7-42ff-8a64-c6a2451f67d4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "167d6d24-6415-42ef-bebc-8d1fd9bc63a5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 5,
            "y": 0
        },
        {
            "id": "0c1216e8-654e-4436-8cd6-172be2b2b609",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 28,
            "y": 0
        },
        {
            "id": "6810d13e-60be-4f81-819e-9efc686d3908",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 28,
            "y": 32
        },
        {
            "id": "b522d28f-d8ef-457c-9d3e-8425b823a920",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 5,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "0b046aab-4c33-4934-95c4-f476b527205f",
    "visible": true
}