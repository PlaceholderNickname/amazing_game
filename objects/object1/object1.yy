{
    "id": "4d37b7db-a914-44bf-aa87-5e40aec1108a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object1",
    "eventList": [
        {
            "id": "9c5ae7b7-5c17-41af-b29a-cc3376ededf3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "4d57fd59-bcc7-42ff-8a64-c6a2451f67d4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4d37b7db-a914-44bf-aa87-5e40aec1108a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "c0458ae9-b78d-4e8a-9f6e-cb2b49bdcb20",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 3,
            "y": 1
        },
        {
            "id": "8de77412-bdfb-4f6a-8bfa-bfee2672d6e9",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 31,
            "y": 1
        },
        {
            "id": "d26c641e-4b50-4595-87d0-a1b0dd4092f6",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 31,
            "y": 30
        },
        {
            "id": "fb6a9170-8bc8-4ce3-809a-a851a49b2889",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 3,
            "y": 30
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f301f6ce-2d30-4808-a979-4bc3885e323b",
    "visible": true
}